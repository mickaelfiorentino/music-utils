#!/bin/bash
#
# This script finds the tempo in a list of mp3 files
# and rename them with the tempo at the beginning
#
for f in *.mp3
do
    echo "$f"
    fname="$(basename "$f" .mp3)"
    ffmpeg -hide_banner -loglevel panic -i "$f" "${fname}.ogg"
    bpm=$(bpm-tag -m 25 -x 100 -n "${fname}.ogg" 2>&1 | grep -E -o '[0-9]+\.[0-9]+')
    b="${bpm} BPM ${f}"
    mv "$f" "$b"
    rm "${fname}.ogg"
done
