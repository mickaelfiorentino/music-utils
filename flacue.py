#!/usr/bin/env python
#
# Note about INDEX : A number between 00 and 99. Index points are specified in MM:SS:FF format, and are
# relative to the start of the file currently referenced. MM is the number of minutes, SS the number
# of seconds, and FF the number of frames (there are seventy five frames to one second). INDEX 01
# commands specify the beginning of a new track. INDEX 00 commands specify the pre-gap of a track;
# you may notice your Audio CD player count up from a negative value before beginning a new track -
# this is the period between INDEX 00 and INDEX 01.

import sys
from subprocess import PIPE
from ffmpy import FFmpeg

# Read cue file from command argument
cue_file = sys.argv[1]

general = {}
tracks = []
current_file = None

parseTrack = False
data = open(cue_file).read().splitlines()

#
# Parsing cue file
#
for line in data:    
    l = line.strip()

    if not parseTrack:
        if l.startswith('REM GENRE'):
            general['genre'] = ' '.join(l.split(' ')[2:]).replace('"', '')

        if l.startswith('REM DATE'):
            general['date'] = ' '.join(l.split(' ')[2:])

        if l.startswith('PERFORMER'):
            general['artist'] = ' '.join(l.split(' ')[1:]).replace('"', '')

        if l.startswith('TITLE'):
            general['album'] = ' '.join(l.split(' ')[1:]).replace('"', '')

        if l.startswith('FILE'):
            parseTrack = True
            current_file = ' '.join(l.split(' ')[1:-1]).replace('"', '')

    if parseTrack:
        if l.startswith('TRACK'):
            track = general.copy()
            track['track'] = int(l.split(' ')[1], 10)

        if l.startswith('TITLE'):
            track['title'] = ' '.join(l.split(' ')[1:]).replace('"', '')

        if l.startswith('PERFORMER'):
            track['artist'] = ' '.join(l.split(' ')[1:]).replace('"', '')

        if l.startswith('INDEX 01'):
            t = map(int, ' '.join(l.split(' ')[2:]).replace('"', '').split(':'))
            track['start'] = 60 * t[0] + t[1] + t[2] / 75
            tracks.append(track)

for i in range(len(tracks)):
    if i != len(tracks) - 1:
        tracks[i]['duration'] = tracks[i + 1]['start'] - tracks[i]['start']

#
# Getting Metadata & Running FFmpeg
#
for track in tracks:

    metadata = {
        'artist': track['artist'],
        'title': track['title'],
        'album': track['album'],
        'track': str(track['track']) + '/' + str(len(tracks))
    }
    
    if 'genre' in track:
        metadata['genre'] = track['genre']

    if 'date' in track:
        metadata['date'] = track['date']

    input_file  = '%s' % current_file
    output_file = '%.2d - %s.mp3' % (track['track'], track['title']) 

    cmd_args  = '-b:a 320k'
    cmd_args += ' -ss %.2d:%.2d:%.2d' % (track['start'] / 60 / 60, track['start'] / 60 % 60, int(track['start'] % 60))

    if 'duration' in track:
        cmd_args += ' -t %.2d:%.2d:%.2d' % (track['duration'] / 60 / 60, track['duration'] / 60 % 60, int(track['duration'] % 60))

    cmd_args += ' ' + ' '.join('-metadata %s="%s"' % (k, v) for (k, v) in metadata.items())
    
    ff = FFmpeg (
        inputs={input_file: None},
        outputs={output_file: cmd_args}
    )
    
    print ff.cmd
    ff.run(stderr=PIPE)

