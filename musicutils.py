#-------------------------------------------------------------------------------------
# File   : musicutils.py
# Author : Mickael Fiorentino <mickael.fiorentino@mailbox.org>
# Brief  :
#   Classes used to extract music and get metadata
#   https://python-musicbrainzngs.readthedocs.io/en/v0.7.1/api/
#-------------------------------------------------------------------------------------
import musicbrainzngs as mb
import sys
import youtube_dl
from pathlib import Path

class Metadata:
    '''Use musicbrainz API to get metadata of a record'''

    def __init__(self, title, limit):
        '''Login to musicbrainz and search for #title. Return #limit results'''

        # Init contact with API
        mb.set_useragent("musicutils", "0.1", "mickael.fiorentino@mailbox.org")

        # Search MusicBrainz database using the title
        # Limit number of results to the first #limit
        try:
            entries = mb.search_releases(query=title, limit=limit, strict=True)

        except mb.WebServiceError as err:
            print("MusicBrainz search error %s" % err)

        # Exit if the search produced no results or results are empty
        if not (('entries' in locals()) or (len(entries['release-list']) > 0)):
            sys.exit("MusicBrainz search error")

        self.title = title
        self.entries = entries['release-list']
        self.record = {
            'album': 'unknown', 'artist': 'unknown', 'year': '1900', 'id': 0,
            'tracks': []
        }

    def list_entries(self):
        '''List entries returned by musicbrainz search'''

        for e, entry in enumerate(self.entries):
            title  = entry['title']
            name   = entry['artist-credit'][0]['name']
            date   = entry.get('date', '1900')
            ntrack = entry.get('medium-track-count', '0')
            print('{}: {} - {} - {} - {} tr'.format(e, title, name, date, ntrack))

    def make_record(self, idx):
        '''Build the self.record dictionary from self.entries[#idx] information'''

        try:
            mbid = self.entries[idx]['id']

        except IndexError:
            print("Entry %d is out of range" % idx)

        # Search for exact match in MusicBrainz Database
        try:
            release = mb.get_release_by_id(mbid, includes=["artists", "recordings"])

        except mb.WebServiceError as err:
            print("MusicBrainz search error %s" % err)

        else:
            album    = release['release']['title']
            artist   = release['release']['artist-credit'][0]['artist']['name']
            date     = release['release']['date']
            rid      = release['release']['id']
            tr_nb    = release['release']['medium-list'][0].get('track-count', '0')
            tr_list  = release['release']['medium-list'][0]['track-list']

            self.record = {
                'album': album, 'artist': artist, 'year': date, 'id': rid,
                'tracks': [track['recording']['title'] for track in tr_list]
            }

class YTrip:
    '''Extract music from youtube'''

    def __init__(self, url):
        ''' '''

        self.url = url

        # Get raw information
        with youtube_dl.YoutubeDL() as ydl:
            self.info = ydl.extract_info(url, download=False)

        if 'entries' in self.info:
            self.record_type = 'playlist'
            self.record = {
                'title': self.info['entries'][0]['album'],
                'tracks': [t['track'] for t in self.info['entries']]
            }

        elif 'chapters' in self.info:
            self.record_type = 'full_album'
            self.record = {
                'title': self.info['title'],
                'tracks': [t['title'] for t in self.info['chapters']]
            }

        else:
            sys.exit("Got an unknown youtube_dl result")

    def download(self):
        ''' '''

        ydl_opts = {
            'outtmpl': '%(title)s.%(ext)s',
            'format': 'bestaudio/best',
            'postprocessors': [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'mp3',
                'preferredquality': '320',
            }]
        }
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            ydl.download([self.url])
