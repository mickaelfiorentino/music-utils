#!/usr/bin/env python3
#--------------------------------------------------------------------------------------------------
# File   : cdrip.py
# Author : Mickael Fiorentino <mickael.fiorentino@mailbox.org>
# Date   : <2020-02-23 Sun>
# Brief  : Rip CD to MP3 & get metadata from Musicbrainz
#
#    https://fedoramagazine.org/use-gstreamer-python-rip-cds/
#    https://pythonhosted.org/python-libdiscid/index.html
#    https://python-musicbrainzngs.readthedocs.io/en/v0.6/api/
#    https://mutagen.readthedocs.io/en/latest/api/id3.html
#    https://github.com/berdario/mutagen/blob/master/TUTORIAL
#--------------------------------------------------------------------------------------------------
import subprocess
import os
import musicbrainzngs as mb
import discid
from mutagen.easyid3 import EasyID3 as ID3

from Record import Record

if __name__ == "__main__":

    #--------------------------------------------------------------------
    # Get cd information from Musicbrainz
    #--------------------------------------------------------------------

    # Login to musicbrainz
    mb.set_useragent("cdrip", "0.1", contact="mickael.fiorentino@mailbox.org")

    # Read disc information
    disc = discid.read(discid.get_default_device())

    # Make record object
    R = Record(disc.last_track_num, disc.id)

    # Get release from MusicBrainz
    try:
        release = mb.get_releases_by_discid(R.mbid, includes=["artists", "recordings"])
    except mb.WebServiceError as err:
        print("Musicbrainz search error %s" % err)

    # Make record from release & Ask user to proceed
    if 'release' in locals():
        R.make_record_from_release(release)
        proceed = R.validate_record()

    # If release not found from disc id, or results not good, fill title information manually
    if not 'release' in locals() or not proceed:
        R.make_record_title()

        # Search MusicBrainz database (limit results to the first 5):
        try:
            releases = mb.search_releases(query  = R.record['album'],
                                          artist = R.record['artist'],
                                          date   = R.record['year'],
                                          limit=5, strict=True)
        except mb.WebServiceError as err:
            print("MusicBrainz search error %s" % err)

        # Select release from the list
        if 'releases' in locals():
            R.select_release(releases)

        # Get exact release from MusicBrainz using the release id
        try:
            release = mb.get_release_by_id(R.mbid, includes=["artists", "recordings"])
        except mb.WebServiceError as err:
            print("MusicBrainz search error %s" % err)

        # Ask user to proceed
        if 'release' in locals():
            R.make_record_from_release(release)
            proceed = R.validate_record()

    # If release still not found, fill tracks information manually
    if not 'release' in locals() or not proceed:
        R.make_record_tracks()

    #--------------------------------------------------------------------
    # RIP CD & CONVERT TO MP3 WITH GSTREAMER
    #--------------------------------------------------------------------

    # Move to Artist/Album directory
    path = '{}/{}'.format(R.record['artist'], R.record['album'])
    os.makedirs(path, exist_ok=True)
    os.chdir(path)

    # RIP each track
    for t, track in enumerate(R.record['tracks']):
        track_name = '{:02} - {}.mp3'.format(t+1, track).replace('/', '-')
        print('Ripping track {}...'.format(track_name))

        # gstreamer pipeline
        cmd = 'gst-launch-1.0 cdiocddasrc track={}'.format(t+1) \
            + ' ! audioconvert ! lamemp3enc target=bitrate cbr=true bitrate=320 ! id3v2mux' \
            + ' ! filesink location="{}"'.format(track_name)

        msgs = subprocess.getoutput(cmd)

        # Tag metadata
        audio = ID3(track_name)
        audio['TRACKNUMBER'] = str(t+1)
        audio['TITLE']       = track
        audio['ARTIST']      = R.record['artist']
        audio['ALBUM']       = R.record['album']
        audio['DATE']        = R.record['year']
        audio.save()

    os.chdir('../..')
    print('Done.')
