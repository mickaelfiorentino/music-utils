#!/usr/bin/env python3
#--------------------------------------------------------------------------------------------------
# File   : ytrip.py
# Author : Mickael Fiorentino <mickael.fiorentino@mailbox.org>
# Brief  : Rip YouTube to MP3 & get metadata from Musicbrainz
# Links  : https://github.com/ytdl-org/youtube-dl/blob/master/README.md#readme
#          https://python-musicbrainzngs.readthedocs.io/en/v0.6/api/
#          https://mutagen.readthedocs.io/en/latest/api/id3.html
#          https://github.com/berdario/mutagen/blob/master/TUTORIAL
#--------------------------------------------------------------------------------------------------
from __future__ import unicode_literals
import sys
from pathlib import Path
import youtube_dl
import musicbrainzngs as mb
from mutagen.easyid3 import EasyID3 as ID3
import subprocess as sp

YT_URL     = 'https://www.youtube.com/watch?v=81DjBm0GZHY'
OUT_DIR    = Path("/nastapool/media/Downloads/")
FFMPEG_BIN = 'ffmpeg'

#--------------------------------------------------------------------------------------------------
# Get Metadata
#
#    Extract metadata from youtube url using YoutubeDL
#    Search for releases matching the title of the download using MusicBrainz API
#    Let the user choose the result from the list & get the exact match from MusicBrainz
#--------------------------------------------------------------------------------------------------
mb.set_useragent("ytrip", "0.1", contact="mickael.fiorentino@mailbox.org")

# Get raw metadata
with youtube_dl.YoutubeDL() as ydl:
    meta = ydl.extract_info(YT_URL, download=False)

# Search MusicBrainz database using the title (limit results to the first 5)
try:
    entries = mb.search_releases(query=meta['title'], limit=5, strict=True)
except mb.WebServiceError as err:
    print("MusicBrainz search error %s" % err)

# Exit if the search produced no results
if not ('entries' in locals()):
    sys.exit("MusicBrainz search error")

# Exit if the results are empty
if not (len(entries['release-list']) > 0):
    sys.exit("MusicBrainz search error")

# Propose the results to the user
for e, entry in enumerate(entries['release-list']):
    title  = entry['title']
    name   = entry['artist-credit'][0]['name']
    date   = entry.get('date', '1900')
    ntrack = entry.get('medium-track-count', '0')
    print('{}: {} - {} - {} - {} tracks '.format(e, title, name, date, ntrack))

entry = int(input('Which result ? '))
mbid  = entries['release-list'][entry]['id']

# Search for exact match in MusicBrainz Database
try:
    release = mb.get_release_by_id(mbid, includes=["artists", "recordings"])
except mb.WebServiceError as err:
    print("MusicBrainz search error %s" % err)

# Extract usefull information
chapters  = meta['chapters']
album     = release['release']['title']
artist    = release['release']['artist-credit'][0]['artist']['name']
date      = release['release']['date']
rid       = release['release']['id']
tr_nb     = release['release']['medium-list'][0].get('track-count', '0')
tr_list   = release['release']['medium-list'][0]['track-list']
tr_names  = [track['recording']['title'] for track in tr_list]
tr_starts = [track['start_time'] for track in chapters]
tr_ends   = [track['end_time'] for track in chapters]
tr_keys   = ['name', 'start', 'end']

# Make a record
record = {
    'album': album, 'artist': artist, 'year': date, 'id': rid,
    'tracks': [dict(zip(tr_keys, [tr_names[i], tr_starts[i], tr_ends[i]])) for i in range(tr_nb)]
}

#--------------------------------------------------------------------------------------------------
# Download & Convert
#
#    Download raw audio (temporarily) from youtube
#    Chunk the raw file into independent tracks, using metadata information
#    Add metadata tags with ID3 to the tracks
#--------------------------------------------------------------------------------------------------
raw = Path("./raw.mp3")

# Clean
if raw.exists():
    raw.unlink()

# Download with YoutubeDL
ydl_opts = {
    'outtmpl': raw.stem + '.%(ext)s',
    'format': 'bestaudio/best',
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '320',
    }]
}
with youtube_dl.YoutubeDL(ydl_opts) as ydl:
    ydl.download([YT_URL])

# Chunk raw audio by track
for t, track in enumerate(record['tracks']):
    tr_name = str(OUT_DIR / '{} - {}.mp3'.format(t+1, track['name'].replace('/','-')))
    cmd = [FFMPEG_BIN,
           '-y',
           '-i', str(raw),
           '-ss', str(track['start']),
           '-t', str(track['end'] - track['start']),
           '-acodec', 'copy',
           tr_name]
    try:
        exe = sp.run(cmd, check=True, stdout=sp.PIPE, stderr=sp.PIPE, timeout=10)
    except sp.CalledProcessError as err:
        print('FFMPEG error: {}\n{}'.format(err, exe.stdout + exe.stderr))
    except sp.TimeoutExpired as err:
        print('FFMPEG error: {}\n{}'.format(err, exe.stdout + exe.stderr))

    # Tag metadata
    tr_final = ID3(tr_name)
    tr_final['TITLE']       = track['name']
    tr_final['TRACKNUMBER'] = str(t+1)
    tr_final['ARTIST']      = record['artist']
    tr_final['ALBUM']       = record['album']
    tr_final['DATE']        = record['year']
    tr_final.save()

# Clean
if raw.exists():
    raw.unlink()
